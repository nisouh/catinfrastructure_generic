CREATE SEQUENCE unmapped_values_seq;

CREATE TABLE unmapped_values
(
	id bigint NOT NULL DEFAULT nextval('unmapped_values_seq'),
	found_value character varying,
	source_id bigint NOT NULL,
	predicate_id bigint NOT NULL,
	CONSTRAINT unmapped_values_pkey PRIMARY KEY(id),
	CONSTRAINT "unmapped_values_source_fk" FOREIGN KEY (source_id)
      REFERENCES source (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT "unmapped_values_predicate_fk" FOREIGN KEY (predicate_id)
      REFERENCES ontology_predicate (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE OR REPLACE FUNCTION fn_getObjectForValue(IN input_value character varying, IN source_description character varying, IN predicate_label character varying,
												OUT object bigint, OUT predicate bigint, OUT source bigint) AS
$BODY$
-- =============================================
-- Author: Johan van Soest
-- Create date: 28 July 2014
-- Description:	Check whether an object is already available, with a mapping to the given local value.
--     If not available, add the request to the table unmapped_values
-- =============================================

DECLARE

	object_id_found bigint;
	source_id_found bigint;
	unmapped_id bigint;
	predicate_id_found bigint;

BEGIN

SELECT id
INTO predicate_id_found
FROM ontology_predicate
WHERE label = predicate_label;

SELECT id
INTO source_id_found
FROM source
WHERE description = source_description;

SELECT ontology_object_id
INTO object_id_found
FROM ontology_object_synonym
WHERE source_id = source_id_found
  AND synonym LIKE input_value;

IF object_id_found IS NULL THEN
	SELECT id
	INTO unmapped_id
	FROM unmapped_values
	WHERE found_value = input_value
		AND source_id = source_id_found;
	
	IF unmapped_id IS NULL THEN
		INSERT INTO unmapped_values (found_value,source_id,predicate_id) VALUES (input_value, source_id_found, predicate_id_found);
	END IF;
END IF;

object := object_id_found;
predicate := predicate_id_found;
source := source_id_found;

END

$BODY$
  LANGUAGE plpgsql;
