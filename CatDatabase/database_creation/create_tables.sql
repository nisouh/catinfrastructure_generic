-- Create tables
CREATE TABLE patient
(
	id character varying(20) NOT NULL,
	cohort character varying(5) NOT NULL,
	CONSTRAINT "patient_pk" PRIMARY KEY (id)
);

CREATE SEQUENCE ontology_predicate_id_seq;

CREATE TABLE ontology_predicate
(
	id bigint NOT NULL DEFAULT nextval('ontology_predicate_id_seq'),
	uri character varying,
	label character varying,
	CONSTRAINT "ontology_predicate_pk" PRIMARY KEY (id),
	CONSTRAINT "ontology_predicate_unique_uri" UNIQUE (uri)
);

CREATE SEQUENCE ontology_object_id_seq;

CREATE TABLE ontology_object
(
	id bigint NOT NULL DEFAULT nextval('ontology_object_id_seq'),
	uri character varying,
	CONSTRAINT "ontology_object_pk" PRIMARY KEY (id)
);

CREATE SEQUENCE source_id_seq;

CREATE TABLE source
(
	id bigint NOT NULL DEFAULT nextval('source_id_seq'),
	description character varying,
	CONSTRAINT "source_pk" PRIMARY KEY (id)
);

CREATE SEQUENCE ontology_object_synonym_id_seq;

CREATE TABLE ontology_object_synonym
(
	id bigint NOT NULL DEFAULT nextval('ontology_object_synonym_id_seq'),
	ontology_object_id bigint NOT NULL,
	source_id bigint NOT NULL,
	synonym character varying,
	constraint "ontology_object_synonym_pk" PRIMARY KEY (id),
	CONSTRAINT "ontology_object_reference_fk" FOREIGN KEY (ontology_object_id)
		REFERENCES ontology_object (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT "object_source_reference_fk" FOREIGN KEY (source_id)
		REFERENCES source (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE SEQUENCE ontology_unit_id_seq;

CREATE TABLE ontology_unit
(
	id bigint NOT NULL DEFAULT nextval('ontology_unit_id_seq'),
	uri character varying,
	CONSTRAINT "ontology_unit_pk" PRIMARY KEY (id)
);

CREATE SEQUENCE statement_patient_id_seq;

CREATE TABLE statement_patient
(
	id bigint NOT NULL DEFAULT nextval('statement_patient_id_seq'),
	patient_id character varying(20) NOT NULL,
	predicate_reference bigint,
	object_reference bigint,
	object_literal_value character varying,
	object_literal_units bigint,
	source_id bigint,
	datetime_begin timestamp,
	datetime_end timestamp,
	CONSTRAINT "statement_patient_pk" PRIMARY KEY (id),
	CONSTRAINT "statement_patient_fk" FOREIGN KEY (patient_id)
		REFERENCES patient (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT "object_reference_fk" FOREIGN KEY (object_reference)
		REFERENCES ontology_object (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT "predicate_reference_fk" FOREIGN KEY (predicate_reference)
		REFERENCES ontology_predicate (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT "unit_reference_fk" FOREIGN KEY (object_literal_units)
		REFERENCES ontology_unit (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT "statement_source_reference_fk" FOREIGN KEY (source_id)
		REFERENCES source (id) MATCH SIMPLE
		ON UPDATE CASCADE ON DELETE CASCADE
);